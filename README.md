
Installation 
=============

1. Install some pre-requisites:

   ``` sudo apt-get install git-core python-argparse python-wstool python-vcstools python-rosdep ros-kinetic-control-msgs ros-kinetic-joystick-drivers ros-kinetic-xacro ros-kinetic-tf2-ros ros-kinetic-rviz ros-kinetic-cv-bridge ros-kinetic-actionlib ros-kinetic-actionlib-msgs ros-kinetic-dynamic-reconfigure ros-kinetic-trajectory-msgs ros-kinetic-rospy-message-converter ```

2. ``` git clone git@bitbucket.org:traclabs/sawyer_workspace.git ```
3. ```cd sawyer_workspace ```
4. Run ```./install.py``` 
5. ```catkin build```

The above instructions are enough to test Sawyer/Baxter in Rviz:

Running in Rviz
================

Sawyer
------

``` $ roslaunch sawyer_application_tools sawyer_demo.launch sim:=true ```

Baxter
------
 
``` $ roslaunch sawyer_application_tools baxter_demo.launch sim:=true ```

Extra steps to run in Gazebo
============================

Configure Sawyer
----------------
1. Copy the intera.sh script in the workspace root:
   ``` cp src/intera_sdk/intera.sh ~/sawyer_workspace ```
2. Customize the intera.sh script (takes 1 minute). Follow steps here: http://sdk.rethinkrobotics.com/intera/Workstation_Setup, start from "Customize the intera script" section till "Save and close intera script"

Configure Baxter
-----------------
1. Copy the baxter.sh script in the workspace root:
   ``` cp src/baxter/baxter.sh ~/sawyer_workspace ```
2. Customize the baxter.sh script the same way you did for Sawyer


Running in Gazebo
=================

Sawyer
-------
**NOTE:** Remember to source your workspace AND then to run ```./intera.sh sim``` (in every terminal)

Start the robot in Gazebo:

``` $ roslaunch sawyer_gazebo sawyer_world.launch electric_gripper:=true ```

Enable the robot:

``` $ roscd intera_interface/scripts```
``` $ ./robot_enable.py -e ```

The robot does not have the trajectory server started by default, so you'll need to start it:

``` $ rosrun intera_interface joint_trajectory_action_server.py --mode position ```

To run our stuff:

``` $ roslaunch sawyer_application_tools sawyer_demo.launch sim:=false ```

Baxter
-------
**NOTE:** Remember to source your workspace AND then to run ```./baxter.sh sim``` (in every terminal)

Start the robot in Gazebo:

``` $ roslaunch sawyer_application_tools baxter_world.launch ```

Enable the robot:

``` $ roscd baxter_tools/scripts```
``` $ ./robot_enable.py -e ```

The robot does not have the trajectory server started by default, so you'll need to start it:

``` $ rosrun baxter_interface joint_trajectory_action_server.py ``` 

To run our stuff:

``` $ roslaunch sawyer_application_tools baxter_demo.launch sim:=false ```


![sawyer_gazebo](images/sawyer_gazebo.png)


